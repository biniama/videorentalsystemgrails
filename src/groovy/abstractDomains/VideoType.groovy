package abstractDomains

enum VideoType {

	CHILDREN_VIDEO("Children's Video", 8.00),
	REGULAR("Regular", 10.00),
	NEW_RELEASE("New Release", 15.00)
	
	private final String type
	
	private final Double rate
	
	VideoType(String type, Double rate)
	{
		this.type = type
		
		this.rate = rate
	}

	private String type()
	{
		return type
	}
	
	private Double rate()
	{
		return rate
	}

}