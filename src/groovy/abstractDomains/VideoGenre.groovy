package abstractDomains

enum VideoGenre {

	ACTION ("Regular"),
	COMEDY ("Comedy"),
	DRAMA ("Drama"),
	HORROR ("Horror"),
	ADVENTURE ("Adventure"),
	ANIMATION ("Animation"),
	SUSPENSE ("Suspense"),
	CRIME ("Crime"),
	DOCUMENTARY ("Documentary"),
	FAMILY ("Family"),
	DANCE ("Dance"),
	SCIENCE_FICTION ("Science Fiction"),
	ROMANCE ("Romance"),
	HISTORY ("History")
	
	private String genre
	
	VideoGenre(String genre)
	{
		this.genre = genre
	}
	
	private String genre()
	{
		return genre
	}
}
