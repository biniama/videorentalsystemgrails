package videorentalsystem

class User {
	
	String fullName
	
	String userName
	
	String password

    static constraints = {
		
		fullName()
		
		userName(unique : true, blank : false)
		
		password(password : true)
		
    }
}
