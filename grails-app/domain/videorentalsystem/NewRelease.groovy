package videorentalsystem

class NewRelease extends Video {

	Date yearReleased
	
	//Video video
	
	//static belongsTo = [video : Video]
	
    static constraints = {
		
		yearReleased(max: new Date())
    }
	
	String toString()
	{
		"New Release + ${yearReleased}"
	}
}
