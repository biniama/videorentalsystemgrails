package videorentalsystem

class ChildrenVideo extends Video {

	Integer maximumAge
	
	//Video video
	
	//static belongsTo = [video : Video]
	
    static constraints = {
		
		maximumAge (min:1, max:120)
		// maximumAge (range: 1..120)
		
    }
	
	String toString()
	{
		"Children Video + ${maximumAge}"
	}
}
