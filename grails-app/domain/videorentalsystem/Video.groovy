package videorentalsystem

import abstractDomains.VideoGenre;
import abstractDomains.VideoType;

class Video {
	
	String videoTitle
	
	VideoType videoType
	
	VideoGenre videoGenre
	
	//static hasOne = [newReleases : NewRelease, regular : Regular]
	
	String toString()
	{
		"${videoTitle}"
	}
	
	static constraints = {
		
		videoTitle (blank: false, unique: true)
			
		//videoType(inList:["Children's Video", "Regular", "New Release"])
		
		//videoGenre(inList:["Action", "Comedy", "Adventure", "Drama"])
	}
	
//	static mapping = {
//		
//		tablePerHierarchy false
//		
//	}
	
}
