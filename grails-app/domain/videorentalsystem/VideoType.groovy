package videorentalsystem

class VideoType {
	
//	String videoType
//	
//	String toString()
//	{
//		"${videoType}"
//	}
//	
    static constraints = {
	
		videoType(inList:["Children's Video", "Regular", "New Release"])
		
    }
}
