<%@ page import="videorentalsystem.ChildrenVideo" %>



<div class="fieldcontain ${hasErrors(bean: childrenVideoInstance, field: 'videoTitle', 'error')} required">
	<label for="videoTitle">
		<g:message code="childrenVideo.videoTitle.label" default="Video Title" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="videoTitle" required="" value="${childrenVideoInstance?.videoTitle}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: childrenVideoInstance, field: 'maximumAge', 'error')} required">
	<label for="maximumAge">
		<g:message code="childrenVideo.maximumAge.label" default="Maximum Age" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="maximumAge" type="number" min="1" max="120" value="${childrenVideoInstance.maximumAge}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: childrenVideoInstance, field: 'videoGenre', 'error')} required">
	<label for="videoGenre">
		<g:message code="childrenVideo.videoGenre.label" default="Video Genre" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="videoGenre" from="${abstractDomains.VideoGenre?.values()}" keys="${abstractDomains.VideoGenre.values()*.name()}" required="" value="${childrenVideoInstance?.videoGenre?.name()}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: childrenVideoInstance, field: 'videoType', 'error')} required">
	<label for="videoType">
		<g:message code="childrenVideo.videoType.label" default="Video Type" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="videoType" from="${abstractDomains.VideoType?.values()}" keys="${abstractDomains.VideoType.values()*.name()}" required="" value="${childrenVideoInstance?.videoType?.name()}"/>
</div>

