<%@ page import="videorentalsystem.Regular" %>



<div class="fieldcontain ${hasErrors(bean: regularInstance, field: 'videoTitle', 'error')} required">
	<label for="videoTitle">
		<g:message code="regular.videoTitle.label" default="Video Title" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="videoTitle" required="" value="${regularInstance?.videoTitle}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: regularInstance, field: 'videoGenre', 'error')} required">
	<label for="videoGenre">
		<g:message code="regular.videoGenre.label" default="Video Genre" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="videoGenre" from="${abstractDomains.VideoGenre?.values()}" keys="${abstractDomains.VideoGenre.values()*.name()}" required="" value="${regularInstance?.videoGenre?.name()}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: regularInstance, field: 'videoType', 'error')} required">
	<label for="videoType">
		<g:message code="regular.videoType.label" default="Video Type" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="videoType" from="${abstractDomains.VideoType?.values()}" keys="${abstractDomains.VideoType.values()*.name()}" required="" value="${regularInstance?.videoType?.name()}"/>
</div>

