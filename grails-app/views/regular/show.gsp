
<%@ page import="videorentalsystem.Regular" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'regular.label', default: 'Regular')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-regular" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-regular" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list regular">
			
				<g:if test="${regularInstance?.videoTitle}">
				<li class="fieldcontain">
					<span id="videoTitle-label" class="property-label"><g:message code="regular.videoTitle.label" default="Video Title" /></span>
					
						<span class="property-value" aria-labelledby="videoTitle-label"><g:fieldValue bean="${regularInstance}" field="videoTitle"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${regularInstance?.videoGenre}">
				<li class="fieldcontain">
					<span id="videoGenre-label" class="property-label"><g:message code="regular.videoGenre.label" default="Video Genre" /></span>
					
						<span class="property-value" aria-labelledby="videoGenre-label"><g:fieldValue bean="${regularInstance}" field="videoGenre"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${regularInstance?.videoType}">
				<li class="fieldcontain">
					<span id="videoType-label" class="property-label"><g:message code="regular.videoType.label" default="Video Type" /></span>
					
						<span class="property-value" aria-labelledby="videoType-label"><g:fieldValue bean="${regularInstance}" field="videoType"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${regularInstance?.id}" />
					<g:link class="edit" action="edit" id="${regularInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
