
<%@ page import="videorentalsystem.Regular" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'regular.label', default: 'Regular')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-regular" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-regular" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="videoTitle" title="${message(code: 'regular.videoTitle.label', default: 'Video Title')}" />
					
						<g:sortableColumn property="videoGenre" title="${message(code: 'regular.videoGenre.label', default: 'Video Genre')}" />
					
						<g:sortableColumn property="videoType" title="${message(code: 'regular.videoType.label', default: 'Video Type')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${regularInstanceList}" status="i" var="regularInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${regularInstance.id}">${fieldValue(bean: regularInstance, field: "videoTitle")}</g:link></td>
					
						<td>${fieldValue(bean: regularInstance, field: "videoGenre")}</td>
					
						<td>${fieldValue(bean: regularInstance, field: "videoType")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${regularInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
