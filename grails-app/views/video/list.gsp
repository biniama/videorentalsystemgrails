
<%@ page import="videorentalsystem.Video" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'video.label', default: 'Video')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		
		<script type="text/javascript">
	
  			function enableText(checkBool, textId)
  			{
			    textFldObj = document.getElementById(textId);
	    		//Disable the text field
	    		textFldObj.disabled = !checkBool;
	    		//Clear value in the text field
	    		if (!checkBool) { textFldObj.value = ''; }
  			}

		</script>
		
	</head>
	<body>
		<a href="#list-video" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
			</ul>
		</div>
		<div id="list-video" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
						
						<g:sortableColumn property="videoId" title="${message(code: 'video.id.label', default: 'Id')}" />
					
						<g:sortableColumn property="videoTitle" title="${message(code: 'video.videoTitle.label', default: 'Video Title')}" />
					
						<g:sortableColumn property="videoGenre" title="${message(code: 'video.videoGenre.label', default: 'Video Genre')}" />
					
						<g:sortableColumn property="videoType" title="${message(code: 'video.videoType.label', default: 'Video Type')}" />
					
						<g:sortableColumn property="numberOfDays" title="${message(code: 'video.numberOfDays.label', default: 'Number of Days')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${videoInstanceList}" status="i" var="videoInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:checkBox checked="false" name="allVideos" value="${videoInstance.id}" onclick="enableText(this.checked, '${videoInstance.id}');" /></td>
						
						<td><g:link action="show" id="${videoInstance.id}">${fieldValue(bean: videoInstance, field: "videoTitle")}</g:link></td>
					
						<td>${fieldValue(bean: videoInstance, field: "videoGenre")}</td>
					
						<td>${fieldValue(bean: videoInstance, field: "videoType")}</td>
						
						<td><g:textField id="${videoInstance.id}" name="numberOfDays" disabled="true" /> </td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${userInstance?.id}" />
					<g:link class="calcPrice" action="calcPrice" id="${userInstance?.id}"><g:message code="default.button.calcPrice.label" default="Calculate Rent Price" /></g:link>
				</fieldset>
			</g:form>
			
			<div class="pagination">
				<g:paginate total="${videoInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
