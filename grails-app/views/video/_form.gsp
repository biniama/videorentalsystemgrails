<%@ page import="videorentalsystem.Video" %>

<div class="fieldcontain ${hasErrors(bean: videoInstance, field: 'videoTitle', 'error')} required">
	<label for="videoTitle">
		<g:message code="video.videoTitle.label" default="Video Title" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="videoTitle" required="" value="${videoInstance?.videoTitle}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoInstance, field: 'videoGenre', 'error')} required">
	<label for="videoGenre">
		<g:message code="video.videoGenre.label" default="Video Genre" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="videoGenre" from="${abstractDomains.VideoGenre?.g()}" keys="${abstractDomains.VideoGenre.values()*.name()}" required="" value="${videoInstance?.videoGenre?.genre()}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoInstance, field: 'videoType', 'error')} required">
	<label for="videoType">
		<g:message code="video.videoType.label" default="Video Type" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="videoType" from="${abstractDomains.VideoType?.values()}" keys="${abstractDomains.VideoType.values()*.name()}" required="" value="${videoInstance?.videoType?.type()}"/>
</div>

