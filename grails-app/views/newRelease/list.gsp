
<%@ page import="videorentalsystem.NewRelease" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'newRelease.label', default: 'NewRelease')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-newRelease" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-newRelease" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="videoTitle" title="${message(code: 'newRelease.videoTitle.label', default: 'Video Title')}" />
					
						<g:sortableColumn property="yearReleased" title="${message(code: 'newRelease.yearReleased.label', default: 'Year Released')}" />
					
						<g:sortableColumn property="videoGenre" title="${message(code: 'newRelease.videoGenre.label', default: 'Video Genre')}" />
					
						<g:sortableColumn property="videoType" title="${message(code: 'newRelease.videoType.label', default: 'Video Type')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${newReleaseInstanceList}" status="i" var="newReleaseInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${newReleaseInstance.id}">${fieldValue(bean: newReleaseInstance, field: "videoTitle")}</g:link></td>
					
						<td><g:formatDate date="${newReleaseInstance.yearReleased}" /></td>
					
						<td>${fieldValue(bean: newReleaseInstance, field: "videoGenre")}</td>
					
						<td>${fieldValue(bean: newReleaseInstance, field: "videoType")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${newReleaseInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
