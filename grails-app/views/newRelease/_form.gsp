<%@ page import="videorentalsystem.NewRelease" %>



<div class="fieldcontain ${hasErrors(bean: newReleaseInstance, field: 'videoTitle', 'error')} required">
	<label for="videoTitle">
		<g:message code="newRelease.videoTitle.label" default="Video Title" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="videoTitle" required="" value="${newReleaseInstance?.videoTitle}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: newReleaseInstance, field: 'yearReleased', 'error')} required">
	<label for="yearReleased">
		<g:message code="newRelease.yearReleased.label" default="Year Released" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="yearReleased" precision="day"  value="${newReleaseInstance?.yearReleased}"  />
</div>

<div class="fieldcontain ${hasErrors(bean: newReleaseInstance, field: 'videoGenre', 'error')} required">
	<label for="videoGenre">
		<g:message code="newRelease.videoGenre.label" default="Video Genre" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="videoGenre" from="${abstractDomains.VideoGenre?.values()}" keys="${abstractDomains.VideoGenre.values()*.name()}" required="" value="${newReleaseInstance?.videoGenre?.name()}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: newReleaseInstance, field: 'videoType', 'error')} required">
	<label for="videoType">
		<g:message code="newRelease.videoType.label" default="Video Type" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="videoType" from="${abstractDomains.VideoType?.values()}" keys="${abstractDomains.VideoType.values()*.name()}" required="" value="${newReleaseInstance?.videoType?.name()}"/>
</div>

