package videorentalsystem

class VideoController {
    
	def calculatePriceService
	
	def beforeInterceptor = [action:this.&auth]
	
	def scaffold = true
	
	def index()
	{
		redirect(action: "list")
	}
	
	def auth() {
		if(!session.user) {
			redirect(controller:"User", action:"login")
			return false
		}
	}
	
	def calcPrice()
	{
		def totalPriceInstance
		def tp = 222
		
		totalPriceInstance = calculatePriceService.calcPrice(2, 1)
	}
}