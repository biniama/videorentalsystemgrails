package videorentalsystem

class ChildrenVideoController {
	
	def beforeInterceptor = [action:this.&auth]
	
	def scaffold=true
	
	def index() {
		redirect(action: "list") 
	}
	
	def auth() {
		if(!session.user) {
			redirect(controller:"User", action:"login")
			return false
		}
	}
}
